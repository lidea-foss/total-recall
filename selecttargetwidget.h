#ifndef SELECTTARGETWIDGET_H
#define SELECTTARGETWIDGET_H

#include "lableddiskpartition.h"
#include "mainwindowstates.h"

#include <QWidget>

class QAbstractButton;

namespace Ui {
class SelectTargetWidget;
}

class SelectTargetWidget : public QWidget
{
        Q_OBJECT

    public:
        explicit SelectTargetWidget(QWidget *parent = nullptr);
        bool populateMounts();
        ~SelectTargetWidget();

        bool isTargetSelected() const;
        QString targetUuid() const;

    public slots:
        void btnNextPressed();


    signals:
        void nextState(MainWindoState nextState);
        void TargetWidgetSelected();
        void setNextButtonEnabled(bool enable);

    private:
        Ui::SelectTargetWidget *ui;
        QList<LabledDiskPartition>      mPartitions;
        QString mSelectedUuid;

};

#endif // SELECTTARGETWIDGET_H
