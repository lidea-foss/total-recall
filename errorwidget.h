#ifndef ERRORWIDGET_H
#define ERRORWIDGET_H

#include "mainwindowstates.h"
#include <QWidget>

namespace Ui {
class ErrorWidget;
}

class ErrorWidget : public QWidget
{
        Q_OBJECT

    public:
        explicit ErrorWidget(QWidget *parent = nullptr);
        ~ErrorWidget();

    public slots:
        void btnNextPressed();


    signals:
        void nextState(MainWindoState nextState);

    private:
        Ui::ErrorWidget *ui;
};

#endif // ERRORWIDGET_H
