#ifndef WELCOMEWIDGET_H
#define WELCOMEWIDGET_H

#include "mainwindowstates.h"
#include <QWidget>

namespace Ui {
class WelcomeWidget;
}

class WelcomeWidget : public QWidget
{
        Q_OBJECT

    public:
        explicit WelcomeWidget(QWidget *parent = nullptr);
        ~WelcomeWidget();

    public slots:
        void btnNextPressed();


    signals:
        void nextState(MainWindoState nextState);



    private:
        Ui::WelcomeWidget *ui;
};

#endif // WELCOMEWIDGET_H
