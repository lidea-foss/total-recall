#ifndef RUNNINGDETAILSDIALOG_H
#define RUNNINGDETAILSDIALOG_H

#include <QDialog>

namespace Ui {
class RunningDetailsDialog;
}

class RunningDetailsDialog : public QDialog
{
        Q_OBJECT

    public:
        explicit RunningDetailsDialog(QWidget *parent = nullptr);
        ~RunningDetailsDialog();

    public slots:
        void ttyOutput(QString output);

    private:
        Ui::RunningDetailsDialog *ui;
};

#endif // RUNNINGDETAILSDIALOG_H
