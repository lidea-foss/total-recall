#ifndef RUNBACKUPWIDGET_H
#define RUNBACKUPWIDGET_H

#include "datamodel/datamodel.h"
#include "mainwindowstates.h"
#include "runningdetailsdialog.h"
#include "targetpaths.h"

#include <QWidget>


class BackupDataSet;

namespace Ui {
class RunBackupWidget;
}

class RunBackupWidget : public QWidget
{
        Q_OBJECT

    public:
        explicit RunBackupWidget(QWidget *parent = nullptr);
        ~RunBackupWidget();

        void goForBackup(TargetPaths* paths, DataModel* data);

    protected:

    public slots:
        void btnNextPressed();
        void cancelBtnPressed();


    protected slots:
        void startNextBackup();
        void makeSnapshots();
        void cleanSnapshots();
        void workDone();

    signals:
        void nextState(MainWindoState nextState);
        void setNextButtonEnabled(bool enable);
        void backupDone();

        void statusMessage(const QString& message);
        void logMessage(const QString& message);


    private:
        Ui::RunBackupWidget *ui;


        TargetPaths*                mPaths;
        DataModel*                  mDataModel;
        int                         mModelIndex;
        BackupDataSet*              mCurrentBackupJob;
};

#endif // RUNBACKUPWIDGET_H
