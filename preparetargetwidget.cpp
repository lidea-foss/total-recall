#include "btrfs.h"
#include "preparetargetwidget.h"
#include "ui_preparetargetwidget.h"
#include "statusandnamewidget.h"

#include <QProcess>
#include <QTemporaryDir>

#include <QtDebug>

PrepareTargetWidget::PrepareTargetWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::PrepareTargetWidget),
    bStatusVerifiedOK(false)
{
    ui->setupUi(this);
    ui->pIsMounted->setLabel(tr("Taget is mounted"));
    ui->pHasBaseSnap->setLabel(tr("Target has base directory"));
    ui->pHasTargetFolders->setLabel(tr("Target has target folders"));

}

PrepareTargetWidget::~PrepareTargetWidget()
{
    delete ui;
}

void PrepareTargetWidget::checkAndVerify(const QString &targetDevice, TargetPaths &paths)
{

    emit setNextButtonEnabled(false);

    bool bOk = true;
    if ((bOk = mountAndVerify(targetDevice, paths)))
        ui->pIsMounted->setChecked();
    else
        ui->pIsMounted->setFailed();

    if (bOk && (bOk = checkCreateBaseDir(paths)))
        ui->pHasBaseSnap->setChecked();
    else
        ui->pHasBaseSnap->setFailed();

    if (bOk && (bOk = checkCreateTargetDirs(paths)))
        ui->pHasTargetFolders->setChecked();
    else
        ui->pHasTargetFolders->setFailed();


    bStatusVerifiedOK = bOk;

    if (bStatusVerifiedOK)
    {
        emit checkedAndVerifiedOk();
    }

    emit setNextButtonEnabled(true);    // We can always go next but it might be to the error page.
}

bool PrepareTargetWidget::isStatusVerifiedOk()
{
    return bStatusVerifiedOK;
}

bool PrepareTargetWidget::mountAndVerify(const QString &targetDevice, TargetPaths &paths)
{

    qDebug() << "Mount point: " << paths.mountPoint();

    QProcess mount;

    mount.setProgram("mount");

    QStringList args;

    args << QString("UUID=%1").arg(targetDevice);
    args << paths.mountPoint();

    mount.setArguments(args);
    qDebug() << "About to start " << mount.program() << " with args " << mount.arguments();

    mount.start();
    mount.waitForFinished(120000);

    if (mount.exitStatus() == QProcess::NormalExit && mount.exitCode() == 0)
    {
        qDebug() << "Mounted UUID=" << targetDevice << "semingly correct";


        QProcess mountpoint;

        mountpoint.setProgram("mountpoint");

        args.clear();
        args << "-q" << paths.mountPoint();

        mountpoint.setArguments(args);

        qDebug() << "About to start " << mountpoint.program() << " with args " << mountpoint.arguments();

        mountpoint.start();
        mountpoint.waitForFinished();

        if (mountpoint.exitStatus() == QProcess::NormalExit && mountpoint.exitCode() == 0)
        {
            qDebug() << "Verified that UUID=" << targetDevice << "is mounted";
            return true;
        }
    }

    return false;
}

bool PrepareTargetWidget::checkCreateBaseDir(TargetPaths &paths)
{
    return checkCreatePath(paths.basePath());
}

bool PrepareTargetWidget::checkCreateTargetDirs(TargetPaths &paths)
{
    bool bOk = true;


    bOk = checkCreatePath(paths.currentBasePath());

    if (bOk)
        bOk = checkCreatePath(paths.dailyBasePath());

    if (bOk)
        bOk = checkCreatePath(paths.midTermBasePath());

    if (bOk)
        bOk = checkCreatePath(paths.yearlyBasePath());

    return bOk;
}

bool PrepareTargetWidget::umount(TargetPaths* paths)
{
    qDebug() << "Mount point: " << paths->mountPoint();

    QProcess mount;

    mount.setProgram("umount");

    QStringList args;
    args << paths->mountPoint();

    mount.setArguments(args);

    qDebug() << "About to start " << mount.program() << " with args " << mount.arguments();

    mount.start();
    mount.waitForFinished(120000);

    if (mount.exitStatus() == QProcess::NormalExit && mount.exitCode() == 0)
    {
        qDebug() << "Unmounted " << paths->mountPoint() << "semingly correct";
        return true;
    }

    return false;


}

void PrepareTargetWidget::btnNextPressed()
{
    if (bStatusVerifiedOK)
        emit nextState(State_Backup);
    else
        emit nextState(State_Error);
}

bool PrepareTargetWidget::checkCreatePath(const QString &path)
{
    // Check if the path exists at all
    QDir basePathDir = path;

    // Also verify that it is a valid btrfs sub volume.
    Btrfs   btrfs;

    qDebug() << "Check if " << path << " exists and is a btrfs subvolume";

    if (basePathDir.exists())
    {
        if (btrfs.isPathASubVolume(basePathDir.absolutePath()))
        {
            return true;
        }
        else
        {
            qDebug() << "Path " << path << " exists but is not a subvolume. Need som manual intervension here to fix!";
            return false;
        }
    }

    // Lets create the base subvolume.

    qDebug() << "Create the base dir on target";

    return btrfs.sub_create(basePathDir.absolutePath());
}
