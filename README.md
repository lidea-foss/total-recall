# Total Recall - Linux Dekstop Backup Software

Total Recall aims to be a user friendly and simple to use backup software. It shall be easy enought to be run frequently for a user with low technical experciens. 

## A Wizard Application

The program itself runs as a kind of wizzard that guides the user throught the backup operation;

- Ask the user to attach the backup disk
- Finds the backup partition
- Create the target structure if needed
- Takes the backup
- Unmounts the disk
- Exits the application

There is a summary log view and a detailed log view in separate window. 

## (So far) Hight tech preparations

In order for Total Recall to work there has to be a totalrecall.json to define the backup. As of now, this has to be hand crafted. Example will be posted soon. 

The rational for this right now is that I need a backup program for my users but I still need to help them define what they backup  (the json) so I down prioritized that part. 

## Behind the scene

The backup is made using rsync. 

The backup disk has to be a btrfs disk labeld "TR Backup" something. 

Backups are made to totalrecal-backups/current (writeable btrfs snapshot) using rsync. A Daily read only snapshot is made from current when backup is done. A monthly read only snapshot is made from the daily if it does not already exist for the present month. A yearly readonly snapshot is made from monthly based on the same presmises. 

So there will be:
- current: where rsync operates
- daily: daily snapshots limited to 15 (hardcoded as of now).
- monthly: monthly snapshots, limited to 12 (hardcoded as of now). 
- yearly: yearly snapshots that is not limited.


