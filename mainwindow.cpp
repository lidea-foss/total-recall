#include "mainwindow.h"
#include "errorwidget.h"
#include "ui_mainwindow.h"

#include "finishedwidget.h"
#include "preparetargetwidget.h"
#include "releasetargetwidget.h"
#include "runbackupwidget.h"
#include "runningdetailsdialog.h"
#include "selecttargetwidget.h"
#include "welcomewidget.h"

#include <datamodel/datamodel.h>

#include <QApplication>

#include <QtDebug>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , mState(State_Welcome)
    , ui(new Ui::MainWindow)
    , mCurrent(nullptr)
    , mDetailsDialog(new RunningDetailsDialog(this))
{
    ui->setupUi(this);

//    ui->pBtnCheckForDrives->setEnabled(true);
//    ui->pMount->setEnabled(false);
//    ui->pPrepare->setEnabled(false);
//    ui->pGoForBackup->setEnabled(false);
//    ui->pRelease->setEnabled(false);


//    connect(ui->pBtnCheckForDrives, &QPushButton::clicked, this, &MainWindow::beginScanDisks);


    connect(ui->pBtnDetails, &QPushButton::pressed, this, &MainWindow::viewDetailsBtnPressed);




    QVBoxLayout* layout = new QVBoxLayout;

    mWelcomeWidget = new WelcomeWidget;
    layout->addWidget(mWelcomeWidget);

    mSelectTargetWidget = new SelectTargetWidget;
    layout->addWidget(mSelectTargetWidget);

    mPrepareTargetWidget = new PrepareTargetWidget;
    layout->addWidget(mPrepareTargetWidget);

    mRunBackupWidget = new RunBackupWidget;
    layout->addWidget(mRunBackupWidget);

    mReleaseTargetWidget = new ReleaseTargetWidget;
    layout->addWidget(mReleaseTargetWidget);

    mFinishedWidget = new FinishedWidget;
    layout->addWidget(mFinishedWidget);

    mErrorWidget = new ErrorWidget;
    layout->addWidget(mErrorWidget);

    ui->pFlowBox->setLayout(layout);

	QFileInfo	inFile(QCoreApplication::arguments().at(1));

	if (inFile.exists() && inFile.isFile())
	{


		statusMessage(QString("Using the backupfile %1").arg(inFile.absoluteFilePath()));

		mModel.load(inFile.absoluteFilePath());

		statusMessage("The following backups will be made:");

		for(int index = 0; index < mModel.dataSets().size(); index++)
		{
			statusMessage(QString("    %1 from %2")
						  .arg((mModel.dataSets().at(index))->name())
						  .arg((mModel.dataSets().at(index))->path()));


		}
		disableAll();

		doNextState(State_Welcome);
	}
	else
	{
		statusMessage(QString("Fails to open the backupfile %1").arg(inFile.filePath()));
		doNextState(State_Error);
	}


}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::disableAll()
{
    mWelcomeWidget->setHidden(true);
    mSelectTargetWidget->setHidden(true);
    mPrepareTargetWidget->setHidden(true);
    mRunBackupWidget->setHidden(true);
    mReleaseTargetWidget->setHidden(true);
    mFinishedWidget->setHidden(true);
    mErrorWidget->setHidden(true);

}

void MainWindow::stateWelcome()
{
    ui->pFlowBox->setTitle(tr("Welcome"));

    mWelcomeWidget->setHidden(false);

    connect(ui->pBtnNext, &QPushButton::pressed, mWelcomeWidget, &WelcomeWidget::btnNextPressed);
    connect(mWelcomeWidget, &WelcomeWidget::nextState, this, &MainWindow::doNextState);
    mCurrent = mWelcomeWidget;

}

void MainWindow::stateSelectDevice()
{
    ui->pFlowBox->setTitle(tr("Select Backup Device"));

    mSelectTargetWidget->setHidden(false);

    connect(ui->pBtnNext, &QPushButton::pressed, mSelectTargetWidget, &SelectTargetWidget::btnNextPressed);
    connect(mSelectTargetWidget, &SelectTargetWidget::nextState, this, &MainWindow::doNextState);
    connect(mSelectTargetWidget, &SelectTargetWidget::setNextButtonEnabled, this, &MainWindow::setNextButtonEnabled);
    mCurrent = mSelectTargetWidget;

    mSelectTargetWidget->populateMounts();

}

void MainWindow::statePrepare()
{
    ui->pFlowBox->setTitle(tr("Verify and Prepare Backup Device"));

    mPrepareTargetWidget->setHidden(false);

    connect(ui->pBtnNext, &QPushButton::pressed, mPrepareTargetWidget, &PrepareTargetWidget::btnNextPressed);
    connect(mPrepareTargetWidget, &PrepareTargetWidget::nextState, this, &MainWindow::doNextState);
    connect(mPrepareTargetWidget, &PrepareTargetWidget::setNextButtonEnabled, this, &MainWindow::setNextButtonEnabled);
    mCurrent = mPrepareTargetWidget;

    mPrepareTargetWidget->checkAndVerify(mSelectTargetWidget->targetUuid(),mPaths);

}

void MainWindow::stateBackup()
{
    ui->pFlowBox->setTitle(tr("Run the backup"));

    mRunBackupWidget->setHidden(false);

    connect(ui->pBtnNext, &QPushButton::pressed, mRunBackupWidget, &RunBackupWidget::btnNextPressed);
    connect(mRunBackupWidget, &RunBackupWidget::nextState, this, &MainWindow::doNextState);
    connect(mRunBackupWidget, &RunBackupWidget::setNextButtonEnabled, this, &MainWindow::setNextButtonEnabled);
    connect(mRunBackupWidget, &RunBackupWidget::statusMessage, this, &MainWindow::statusMessage);
    connect(mRunBackupWidget, &RunBackupWidget::logMessage, mDetailsDialog, &RunningDetailsDialog::ttyOutput);
    mCurrent = mRunBackupWidget;

    mRunBackupWidget->goForBackup(&mPaths, &mModel);
}

void MainWindow::stateRelease()
{
    ui->pFlowBox->setTitle(tr("Release the Backup Device"));

    mReleaseTargetWidget->setHidden(false);

    connect(ui->pBtnNext, &QPushButton::pressed, mReleaseTargetWidget, &ReleaseTargetWidget::btnNextPressed);
    connect(mReleaseTargetWidget, &ReleaseTargetWidget::nextState, this, &MainWindow::doNextState);
    connect(mReleaseTargetWidget, &ReleaseTargetWidget::setNextButtonEnabled, this, &MainWindow::setNextButtonEnabled);
    mCurrent = mReleaseTargetWidget;

    mReleaseTargetWidget->releaseTarget(mPrepareTargetWidget, &mPaths);
}

void MainWindow::stateFinished()
{
    ui->pFlowBox->setTitle(tr("Finished!"));

    mFinishedWidget->setHidden(false);

    ui->pBtnNext->setText(tr("Quit"));

    connect(ui->pBtnNext, &QPushButton::pressed, mFinishedWidget, &FinishedWidget::btnNextPressed);
    connect(mFinishedWidget, &FinishedWidget::nextState, this, &MainWindow::doNextState);
    mCurrent = mFinishedWidget;
}

void MainWindow::stateQuit()
{
    QApplication::quit();

}

void MainWindow::stateError()
{
    ui->pFlowBox->setTitle(tr("Error!"));

    mErrorWidget->setHidden(false);

    ui->pBtnNext->setText(tr("Quit"));

    connect(ui->pBtnNext, &QPushButton::pressed, mErrorWidget, &ErrorWidget::btnNextPressed);
    connect(mErrorWidget, &ErrorWidget::nextState, this, &MainWindow::doNextState);
    mCurrent = mErrorWidget;

}

void MainWindow::doNextState(MainWindoState nextState)
{
    disableAll();

    if (mCurrent)
    {
        mCurrent->setHidden(true);
        disconnect(mCurrent);
        mCurrent->disconnect(this);
    }

    switch (nextState)
    {
        case State_Welcome:
            stateWelcome();
            break;
        case State_SelectDevice:
            stateSelectDevice();
            break;

        case State_Prepare:
            statePrepare();
            break;

        case State_Backup:
            stateBackup();
            break;

        case State_Relese:
            stateRelease();
            break;

        case State_Finished:
            stateFinished();
            break;


        case State_Error:
            stateError();
            break;

        case State_Quit:
            stateQuit();
            break;


    }
}

void MainWindow::setNextButtonEnabled(bool enable)
{
    ui->pBtnNext->setEnabled(enable);

}

void MainWindow::viewDetailsBtnPressed()
{
    mDetailsDialog->show();
}

void MainWindow::statusMessage(const QString& message)
{
    ui->pSummary->append(message);

}


void MainWindow::beginScanDisks()
{
//    ui->pBtnCheckForDrives->setEnabled(false);
//    ui->pMount->setEnabled(true);
//    disconnect(ui->pBtnCheckForDrives, &QPushButton::clicked, this, &MainWindow::beginScanDisks);
//    connect(ui->pMount, &SelectTargetWidget::TargetWidgetSelected, this, &MainWindow::beginPreparations);
//    ui->pMount->populateMounts();
}

void MainWindow::beginPreparations()
{
//    ui->pMount->setEnabled(false);
//    ui->pPrepare->setEnabled(true);
//    disconnect(ui->pMount, &SelectTargetWidget::TargetWidgetSelected, this, &MainWindow::beginPreparations);
//    connect(ui->pPrepare, &PrepareTargetWidget::checkedAndVerifiedOk, this, &MainWindow::goForBackup);
//    if (ui->pMount->isTargetSelected())
//        ui->pPrepare->checkAndVerify(ui->pMount->targetUuid(),
//                                     mPaths);

}

void MainWindow::goForBackup()
{
//    qDebug() << "Go for backup";
//    ui->pGoForBackup->setEnabled(true);
//    disconnect(ui->pPrepare, &PrepareTargetWidget::checkedAndVerifiedOk, this, &MainWindow::goForBackup);

//    connect(ui->pGoForBackup, &RunBackupWidget::backupDone, this, &MainWindow::releaseTarget);

//    if (ui->pMount->isTargetSelected() && ui->pPrepare->isStatusVerifiedOk())
//    {
//        ui->pGoForBackup->goForBackup(&mPaths, &mModel);
//    }

}

void MainWindow::releaseTarget()
{
//    ui->pRelease->setEnabled(true);
//    ui->pRelease->releaseTarget(ui->pPrepare, &mPaths);
}

