#include "runningdetailsdialog.h"
#include "ui_runningdetailsdialog.h"

RunningDetailsDialog::RunningDetailsDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::RunningDetailsDialog)
{
    ui->setupUi(this);
    connect(ui->pBtnClose, &QPushButton::pressed, this, &RunningDetailsDialog::hide);
}

RunningDetailsDialog::~RunningDetailsDialog()
{
    delete ui;
}

void RunningDetailsDialog::ttyOutput(QString output)
{

    ui->pDetailsView->appendPlainText(output);

}
