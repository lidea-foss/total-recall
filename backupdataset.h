#ifndef BACKUPDATASET_H
#define BACKUPDATASET_H

#include <QObject>
#include <QProcess>
#include <QTemporaryDir>

class TargetPaths;
class DataSet;

class BackupDataSet : public QObject
{
        Q_OBJECT
    public:
        explicit BackupDataSet(TargetPaths* paths, DataSet* dataSet, QObject *parent = nullptr);


    public slots:
        void start();
        void cancel();

    protected:

        bool createExcludeList();

    protected slots:
        void readStandardOutput();
        void processFinished(int exitCode, QProcess::ExitStatus exitStatus);


    signals:
        void ttyOutput(QString output);
        void finishedOk();
        void failed();


    private:
        DataSet*        mDataSet;
        TargetPaths*    mPaths;
        QProcess        mProcess;
        QTemporaryDir   mTmpDir;

};

#endif // BACKUPDATASET_H
