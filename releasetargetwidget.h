#ifndef RELEASETARGETWIDGET_H
#define RELEASETARGETWIDGET_H

#include "preparetargetwidget.h"
#include <QWidget>

namespace Ui {
class ReleaseTargetWidget;
}

class ReleaseTargetWidget : public QWidget
{
        Q_OBJECT

    public:
        explicit ReleaseTargetWidget(QWidget *parent = nullptr);
        ~ReleaseTargetWidget();

    public slots:
        void releaseTarget(PrepareTargetWidget* prepareWidget, TargetPaths* paths);
        void btnNextPressed();

    signals:
        void nextState(MainWindoState nextState);
        void setNextButtonEnabled(bool enable);

    private:
        Ui::ReleaseTargetWidget*    ui;
        PrepareTargetWidget*        mPrepareWidget;
};

#endif // RELEASETARGETWIDGET_H
