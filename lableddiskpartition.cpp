#include "lableddiskpartition.h"

#include <QStringList>


LabledDiskPartition::LabledDiskPartition(const QString &lsblkInfo)
{

    QStringList diskInfo = lsblkInfo.split(" ");

    device      = diskInfo[0];
    lable       = diskInfo[1].replace("\\x20", " ");
    size        = diskInfo[2];
    uuid        = diskInfo[3];
    viewLable   = QString("%1 (%2, UUID=%3)").arg(lable).arg(device).arg(uuid);


}

LabledDiskPartition::LabledDiskPartition(const LabledDiskPartition &other)
{

    (*this) = other;
}

LabledDiskPartition &LabledDiskPartition::operator =(const LabledDiskPartition &other)
{
    device      = other.device;
    lable       = other.lable;
    size        = other.size;
    uuid        = other.uuid;
    viewLable   = other.viewLable;

    return *this;

}
