#ifndef LABLEDDISKPARTITION_H
#define LABLEDDISKPARTITION_H

#include <QString>



class LabledDiskPartition
{
    public:
        LabledDiskPartition(const QString& lsblkInfo);
        LabledDiskPartition(const LabledDiskPartition& other);

        LabledDiskPartition& operator = (const LabledDiskPartition& other);


    public:

        QString     device;
        QString     lable;
        QString     size;
        QString     uuid;
        QString     viewLable;
};

#endif // LABLEDDISKPARTITION_H
