#ifndef STATUSANDNAMEWIDGET_H
#define STATUSANDNAMEWIDGET_H

#include <QWidget>

namespace Ui {
class StatusAndNameWidget;
}

class StatusAndNameWidget : public QWidget
{
    Q_OBJECT

public:
    explicit StatusAndNameWidget(QWidget *parent = nullptr);
    ~StatusAndNameWidget();

    void setLabel(const QString &label);

public slots:
    void setChecked();
    void setUnChecked();
    void setFailed();



private:
    Ui::StatusAndNameWidget *ui;
};

#endif // STATUSANDNAMEWIDGET_H
