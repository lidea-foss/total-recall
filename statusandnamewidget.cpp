#include "statusandnamewidget.h"
#include "ui_statusandnamewidget.h"

#include <QtSvg>
#include <QPixmap>


StatusAndNameWidget::StatusAndNameWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::StatusAndNameWidget)
{
    ui->setupUi(this);
    ui->pToolBtn->hide();
    setUnChecked();

}

StatusAndNameWidget::~StatusAndNameWidget()
{
    delete ui;
}

void StatusAndNameWidget::setLabel(const QString& label)
{
    ui->pLabel->setText(label);
}

void StatusAndNameWidget::setChecked()
{
    ui->pIcon->setPixmap(QPixmap(":/icons/checkbox_checked-24x24"));
}

void StatusAndNameWidget::setUnChecked()
{
    ui->pIcon->setPixmap(QPixmap(":/icons/checkbox_unchecked-24x24"));
}

void StatusAndNameWidget::setFailed()
{
    ui->pIcon->setPixmap(QPixmap(":/icons/checkbox_failed-24x24"));
}
