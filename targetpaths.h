#include <QDir>
#include <QString>
#include <QTemporaryDir>

#ifndef TARGETPATHS_H
#define TARGETPATHS_H

/**
 * @todo write docs
 */
class TargetPaths
{

    // <mountpoint>/totalrecal-backups/<dataset>/<{current|daily|midterm|yeraly>


    // <mountpoint>/totalrecal-backups/<{current|daily|midterm|yeraly>/<dataset> (subdir, not a snapshot)


    public:
        // enum  MIDTERM { Month = 0, Quartet, Tertial} ;

    public:
        TargetPaths();
        ~TargetPaths();

        bool isValid(); // Valid = mountpoint set.

        const QString mountPoint() const;

        QString baseDirName() const;
        QString basePath() const;
        QDir    baseDir() const;

        QString currentDirName() const;
        QString currentBasePath() const;

        QString rSyncTargetPath(const QString& dataSet) const;

        QString dailyDirName() const;
        QString dailyBasePath() const;

        QString midTermDirName() const;
        QString midTermBasePath() const;

        QString yearlyDirName() const;
        QString yearlyBasePath() const;


    private:
        QDir            mMountPoint;
        QTemporaryDir   mTmpDir;



};

#endif // TARGETPATHS_H
