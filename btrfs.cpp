#include "btrfs.h"

#include <QProcess>
#include <QByteArray>
#include <QDir>
#include <QString>
#include <QtDebug>

Btrfs::Btrfs(QObject *parent)
    : QObject{parent}
{

}

bool Btrfs::isPathASubVolume(const QString &path)
{

    QDir parentDir(path);

    QString dirName = parentDir.dirName();
    parentDir.cdUp(); // Check in the parent dir.

    QString parentDirName = parentDir.dirName();


    QProcess cmd;

    cmd.setProgram("btrfs");

    QStringList args;
    args << "sub";
    args << "list";
    args << "-o";
    args << parentDir.absolutePath();

    cmd.setArguments(args);

    qDebug() << "About to start " << cmd.program() << " with args " << cmd.arguments();

    cmd.start();

    cmd.waitForFinished();

    QByteArray buffer;
    buffer.append(cmd.readAll());

    QString sBuffer = QString::fromUtf8(buffer);
    qDebug() << sBuffer;

    QStringList subs = sBuffer.split("\n").filter(QString("%1").arg(dirName));

    qDebug() << "Subs" << subs;

    return subs.isEmpty() == false;


}

bool Btrfs::sub_create(const QString &path)
{

    QProcess cmd;
    bool bCmdOk = false;

    cmd.setProgram("btrfs");

    QStringList args;
    args << "subvolume";
    args << "create";
    args << path;

    cmd.setArguments(args);

    qDebug() << "About to start " << cmd.program() << " with args " << cmd.arguments();

    cmd.start();

    bCmdOk = cmd.waitForFinished();
    qDebug() << "btrfs " << args << " exitstatus " << cmd.exitStatus() << " and returned " << bCmdOk;

    if (cmd.exitStatus() == QProcess::NormalExit)
        return true;

    return false;

}

bool Btrfs::sub_remove(const QString& path)
{
    QProcess cmd;
    bool bCmdOk = false;

    cmd.setProgram("btrfs");

    QStringList args;
    args << "subvolume";
    args << "delete";
    args << path;

    cmd.setArguments(args);

    qDebug() << "About to start " << cmd.program() << " with args " << cmd.arguments();

    cmd.start();

    bCmdOk = cmd.waitForFinished(30000);
    qDebug() << "btrfs " << args << " exitstatus " << cmd.exitStatus() << " and returned " << bCmdOk;

    if (cmd.exitStatus() == QProcess::NormalExit)
        return true;

    return false;

}

QStringList Btrfs::listSubvolumesIn(const QString& path)
{
    QDir searchDir(path);
    QProcess cmd;

    cmd.setProgram("btrfs");

    QStringList args;
    args << "sub";
    args << "list";
    args << "-o";
    args << searchDir.absolutePath();

    cmd.setArguments(args);

    qDebug() << "About to start " << cmd.program() << " with args " << cmd.arguments();

    cmd.start();

    cmd.waitForFinished();

    QByteArray buffer;
    buffer.append(cmd.readAll());

    QString sBuffer = QString::fromUtf8(buffer);
    qDebug() << sBuffer;


    QStringList result;

    foreach(QString sub, sBuffer.split("\n"))
    {
        QDir subDir(sub);
        if (subDir.dirName() != ".")
            result.append(subDir.dirName());
    }

    qDebug() << "Adjusted subs" << result;

    return result;


}

bool Btrfs::makeSnapshot(const QString& sourcePath, const QString& snapshotPath, bool readOnly)
{
    QProcess cmd;

    cmd.setProgram("btrfs");

    QStringList args;
    args << "sub";
    args << "snap";

    if (readOnly)
        args << "-r";

    args << sourcePath;
    args << snapshotPath;

    cmd.setArguments(args);

    qDebug() << "About to start " << cmd.program() << " with args " << cmd.arguments();

    cmd.start();

    return cmd.waitForFinished();

}
