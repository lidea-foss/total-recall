// <one line to give the program's name and a brief idea of what it does.>
// SPDX-FileCopyrightText: 2022 <copyright holder> <email>
// SPDX-License-Identifier: GPL-3.0-or-later

#include "targetpaths.h"

#include <QApplication>


TargetPaths::TargetPaths()
    :mTmpDir("/tmp/totalrecall-XXXXXX")
{
    if (QApplication::arguments().contains("--keep"))
        mTmpDir.setAutoRemove(false);
}


TargetPaths::~TargetPaths()
{
    // Nothing to do here as of now.
}

bool TargetPaths::isValid()
{
    return true;
}

const QString TargetPaths::mountPoint() const
{
    return mTmpDir.path();
}

QString TargetPaths::baseDirName() const
{
    return QStringLiteral("totalrecal-backups");
}

QString TargetPaths::basePath() const
{
    return mTmpDir.filePath(baseDirName());
}

QDir TargetPaths::baseDir() const
{
    return QDir(basePath());

}

QString TargetPaths::currentDirName() const
{
    return QStringLiteral("current");
}

QString TargetPaths::currentBasePath() const
{
    return baseDir().absoluteFilePath(currentDirName());
}

QString TargetPaths::rSyncTargetPath(const QString &dataSet) const
{
    return QDir(currentBasePath()).absoluteFilePath(dataSet);
}

QString TargetPaths::dailyDirName() const
{
    return QStringLiteral("daily");

}

QString TargetPaths::dailyBasePath() const
{
    return baseDir().absoluteFilePath(dailyDirName());
}

QString TargetPaths::midTermDirName() const
{
    return QStringLiteral("monthly");
}

QString TargetPaths::midTermBasePath() const
{
    return baseDir().absoluteFilePath(midTermDirName());

}

QString TargetPaths::yearlyDirName() const
{
    return QStringLiteral("yearly");

}

QString TargetPaths::yearlyBasePath() const
{
    return baseDir().absoluteFilePath(yearlyDirName());
}

