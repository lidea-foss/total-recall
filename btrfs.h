#ifndef BTRFS_H
#define BTRFS_H

#include <QObject>

class Btrfs : public QObject
{
    Q_OBJECT
public:
    explicit Btrfs(QObject *parent = nullptr);

    bool isPathASubVolume(const QString& path);

    bool sub_create(const QString& path);
    bool sub_remove(const QString& path);

    QStringList listSubvolumesIn(const QString& path);
    bool makeSnapshot(const QString& sourcePath, const QString& snapshotPath, bool readOnly = false);

signals:

};

#endif // BTRFS_H
