QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets svg

CONFIG += c++17

# You can make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    backupdataset.cpp \
    btrfs.cpp \
    datamodel/datamodel.cpp \
    datamodel/dataset.cpp \
    errorwidget.cpp \
    finishedwidget.cpp \
    lableddiskpartition.cpp \
    main.cpp \
    mainwindow.cpp \
    preparetargetwidget.cpp \
    releasetargetwidget.cpp \
    runbackupwidget.cpp \
    runningdetailsdialog.cpp \
    selecttargetwidget.cpp \
    statusandnamewidget.cpp \
    targetpaths.cpp \
    welcomewidget.cpp \
    wizardmanager.cpp

HEADERS += \
    backupdataset.h \
    btrfs.h \
    datamodel/datamodel.h \
    datamodel/dataset.h \
    errorwidget.h \
    finishedwidget.h \
    lableddiskpartition.h \
    mainwindow.h \
    mainwindowstates.h \
    preparetargetwidget.h \
    releasetargetwidget.h \
    runbackupwidget.h \
    runningdetailsdialog.h \
    selecttargetwidget.h \
    statusandnamewidget.h \
    targetpaths.h \
    welcomewidget.h \
    wizardmanager.h

FORMS += \
    errorwidget.ui \
    finishedwidget.ui \
    mainwindow.ui \
    preparetargetwidget.ui \
    releasetargetwidget.ui \
    runbackupwidget.ui \
    runningdetailsdialog.ui \
    selecttargetwidget.ui \
    statusandnamewidget.ui \
    welcomewidget.ui

TRANSLATIONS += \
    totalrecall_en_US.ts
CONFIG += lrelease
CONFIG += embed_translations

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

RESOURCES += \
    resources.qrc
