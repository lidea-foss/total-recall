#include "releasetargetwidget.h"
#include "ui_releasetargetwidget.h"

ReleaseTargetWidget::ReleaseTargetWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::ReleaseTargetWidget)
{
    ui->setupUi(this);
    ui->pUnmount->setLabel("Release the backup disk");
    ui->pUnmount->setUnChecked();

}

ReleaseTargetWidget::~ReleaseTargetWidget()
{
    delete ui;
}

void ReleaseTargetWidget::releaseTarget(PrepareTargetWidget* prepareWidget, TargetPaths* paths)
{
    bool bOk = false;

    if (prepareWidget)
        bOk = prepareWidget->umount(paths);

    if( bOk )
        ui->pUnmount->setChecked();
    else
        ui->pUnmount->setFailed();

}

void ReleaseTargetWidget::btnNextPressed()
{

    emit nextState(State_Finished);

}
