#include "mainwindow.h"

#include <QApplication>
#include <QLocale>
#include <QTranslator>

#include <QtDebug>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

	qDebug() << "ArgC = " << a.arguments().size();
	qDebug() << "ArgV = " << a.arguments();

	if (a.arguments().size() == 2)
	{
		QTranslator translator;
		const QStringList uiLanguages = QLocale::system().uiLanguages();
		for (const QString &locale : uiLanguages) {
			const QString baseName = "totalrecall_" + QLocale(locale).name();
			if (translator.load(":/i18n/" + baseName)) {
				a.installTranslator(&translator);
				break;
			}
		}
		MainWindow w;
		w.show();
		return a.exec();
	}
	else
	{
		qInfo() << "Usage: ";
		qInfo() << "     totalrecall <backupfile.json>";
		qInfo() << "";
		qInfo() << "totalrecall is intended to be run as root to operate properly";
		qInfo() << "";
		qCritical() << "Wrong number of arguments, fails to continue";
	}
}
