#ifndef FINISHEDWIDGET_H
#define FINISHEDWIDGET_H

#include "mainwindowstates.h"
#include <QWidget>

namespace Ui {
class FinishedWidget;
}

class FinishedWidget : public QWidget
{
        Q_OBJECT

    public:
        explicit FinishedWidget(QWidget *parent = nullptr);
        ~FinishedWidget();

    public slots:
        void btnNextPressed();


    signals:
        void nextState(MainWindoState nextState);

    private:
        Ui::FinishedWidget *ui;
};

#endif // FINISHEDWIDGET_H
