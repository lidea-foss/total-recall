#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include "mainwindowstates.h"
#include "targetpaths.h"

#include <QMainWindow>

#include "datamodel/datamodel.h"

class WelcomeWidget;
class SelectTargetWidget;
class PrepareTargetWidget;
class RunBackupWidget;
class ReleaseTargetWidget;
class FinishedWidget;
class ErrorWidget;
class RunningDetailsDialog;


QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
        Q_OBJECT


    public:
        MainWindow(QWidget *parent = nullptr);
        ~MainWindow();
        void run();

    protected:
        void disableAll();
        void stateWelcome();
        void stateSelectDevice();
        void statePrepare();
        void stateBackup();
        void stateRelease();
        void stateFinished();
        void stateQuit();


        void stateError();

    public slots:

        void doNextState(MainWindoState nextState);
        void setNextButtonEnabled(bool enable);

        void viewDetailsBtnPressed();


        void statusMessage(const QString& message);


        void beginScanDisks();
        void beginPreparations();
        void goForBackup();
        void releaseTarget();

    protected:



    private:
        MainWindoState  mState;
        Ui::MainWindow *ui;
        DataModel       mModel;
        TargetPaths     mPaths;

        WelcomeWidget*          mWelcomeWidget;
        SelectTargetWidget*     mSelectTargetWidget;
        PrepareTargetWidget*    mPrepareTargetWidget;
        RunBackupWidget*        mRunBackupWidget;
        ReleaseTargetWidget*    mReleaseTargetWidget;
        FinishedWidget*         mFinishedWidget;
        ErrorWidget*            mErrorWidget;

        QWidget*                mCurrent;

        RunningDetailsDialog*   mDetailsDialog;



};
#endif // MAINWINDOW_H
