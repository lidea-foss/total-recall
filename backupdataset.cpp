#include "backupdataset.h"
#include "targetpaths.h"

#include "datamodel/dataset.h"

#include <QApplication>

const QString   SExcludeFileName = "exclude.lst";

BackupDataSet::BackupDataSet(TargetPaths* paths, DataSet* dataSet, QObject *parent)
    : QObject{parent}
    , mDataSet(dataSet)
    , mPaths(paths)
    , mTmpDir("/tmp/totalrecalljobb-XXXXXX")
{

    if (QApplication::arguments().contains("--keep"))
        mTmpDir.setAutoRemove(false);           // For debuging purposes.

    mProcess.setProgram("rsync");

    QStringList args;

    args << "-av" << "--del";

//    // Dryrun as of now!
//    args << "-n";

    if (!mDataSet->rSyncFlags().isEmpty())
        args << mDataSet->rSyncFlags();

    if (createExcludeList())
        args << QString("--exclude-from=%1").arg(mTmpDir.filePath(SExcludeFileName));

    // Source path
    args << mDataSet->path();

    // Target path
    QDir targetPath(mPaths->currentBasePath());

    args << targetPath.absoluteFilePath(mDataSet->name());

    mProcess.setArguments(args);

    qDebug() << "Backup to run: ";
    qDebug() << "    program  : " << mProcess.program();
    qDebug() << "    arguments: " << mProcess.arguments();

}

void BackupDataSet::start()
{
    // Do nothing as of now.

    emit ttyOutput("Starting backup: ");
    emit ttyOutput(QString("    program  : %1").arg(mProcess.program()));
    emit ttyOutput(QString("    arguments: %1").arg(mProcess.arguments().join(" ")));

    connect(&mProcess, &QProcess::readyRead, this, &BackupDataSet::readStandardOutput);
    connect(&mProcess, QOverload<int, QProcess::ExitStatus>::of(&QProcess::finished), this, &BackupDataSet::processFinished);

    mProcess.start();

}

void BackupDataSet::cancel()
{
    if (mProcess.state() != QProcess::NotRunning)
        mProcess.kill();


}

bool BackupDataSet::createExcludeList()
{
    if (!mDataSet->excludeList().isEmpty())
    {
        QString excludeFileName = mTmpDir.filePath(SExcludeFileName);

        QFile excludeFile(excludeFileName);
        excludeFile.open(QIODevice::WriteOnly);

        foreach(QString line, mDataSet->excludeList())
        {
            excludeFile.write(line.toUtf8());
            excludeFile.write("\n");
        }

        return true;
    }


    return false;

}

void BackupDataSet::readStandardOutput()
{

    QByteArray buffer = mProcess.readAll();
    emit ttyOutput(QString::fromUtf8(buffer));

}

void BackupDataSet::processFinished(int exitCode, QProcess::ExitStatus exitStatus)
{

    emit ttyOutput(QString("Backup is finished with status: %1").arg(exitCode));
    emit ttyOutput(QString("    program      : %1").arg(mProcess.program()));
    emit ttyOutput(QString("    arguments    : %1").arg(mProcess.arguments().join(" ")));
    emit ttyOutput(QString("    exit code    : %1").arg(exitCode));
    emit ttyOutput(QString("    exist status : %1").arg(exitStatus));


    if (exitCode == 0 && exitStatus == QProcess::NormalExit)
        emit  finishedOk();
    else
        emit failed();

}
