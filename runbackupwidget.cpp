#include "backupdataset.h"
#include "btrfs.h"
#include "runbackupwidget.h"
#include "ui_runbackupwidget.h"

RunBackupWidget::RunBackupWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::RunBackupWidget),
    mModelIndex(-1),
    mCurrentBackupJob(nullptr)
{
    ui->setupUi(this);
    ui->pDoneLabel->setHidden(true);
}

RunBackupWidget::~RunBackupWidget()
{
    delete ui;

}

void RunBackupWidget::goForBackup(TargetPaths* paths, DataModel* data)
{
    Q_ASSERT(paths != nullptr);
    Q_ASSERT(data != nullptr);

    emit setNextButtonEnabled(false);

    if (paths && data)
    {
        mPaths = paths;
        mDataModel = data;

        mModelIndex = -1;

        emit statusMessage("The following backups will be made:");

        for(int index = 0; index < data->dataSets().size(); index++)
        {
            emit statusMessage(QString("    %1 from %2")
                                 .arg((data->dataSets().at(index))->name())
                                 .arg((data->dataSets().at(index))->path()));


        }

        startNextBackup();

    }
    else
    {
        emit statusMessage("Internal error. Terminating.");
        emit nextState(State_Error);
    }

}


void RunBackupWidget::cancelBtnPressed()
{
    qDebug() << "Terminating the ongoing process";
    if (mCurrentBackupJob)
        mCurrentBackupJob->cancel();

    emit statusMessage("User pressed cancle and the ongoing job is terminated");


}

void RunBackupWidget::btnNextPressed()
{
    emit nextState(State_Relese);
}


void RunBackupWidget::startNextBackup()
{

    if (mCurrentBackupJob)
    {
        qDebug() << QString("Backup jobb %1 finished OK.").arg(mDataModel->dataSets().at(mModelIndex)->name());
        emit statusMessage(QString("Backup jobb %1 finished OK.").arg(mDataModel->dataSets().at(mModelIndex)->name()));

        delete mCurrentBackupJob;
        mCurrentBackupJob = nullptr;
    }

    mModelIndex++;

    if (mModelIndex < mDataModel->dataSets().size())
    {
        qDebug() << "Not the first jobb, nor the last - increase iterator";


        qDebug() << QString("Fire off backup jobb: %1").arg(mDataModel->dataSets().at(mModelIndex)->name());
        emit statusMessage(QString("Fire off backup jobb: %1").arg(mDataModel->dataSets().at(mModelIndex)->name()));

        mCurrentBackupJob = new BackupDataSet(mPaths, mDataModel->dataSets().at(mModelIndex));
        connect(mCurrentBackupJob, &BackupDataSet::finishedOk, this, &RunBackupWidget::startNextBackup);
        connect(mCurrentBackupJob, &BackupDataSet::failed, this, &RunBackupWidget::workDone);
        connect(mCurrentBackupJob, &BackupDataSet::ttyOutput, this, &RunBackupWidget::logMessage);
        mCurrentBackupJob->start();

    }
    else
    {
        qDebug() << "No more DataSets, work is done";
        makeSnapshots();
        cleanSnapshots();
        workDone();
        return;
    }



}

void RunBackupWidget::makeSnapshots()
{
    Btrfs btrfs;


    QString dailySnap = QDateTime::currentDateTime().toString(Qt::ISODate);

    emit statusMessage(QString("Creating read-only snap in daily (%1)").arg(dailySnap));

    btrfs.makeSnapshot(mPaths->currentBasePath(),
                       QDir(mPaths->dailyBasePath()).absoluteFilePath(dailySnap),
                       true /* read only */ );


    // Check if snap shall be made in midterm

    QString midTermSnap = QDate::currentDate().toString("yyyy-MM");

    if (!btrfs.isPathASubVolume(QDir(mPaths->midTermBasePath()).absoluteFilePath(midTermSnap)))
    {
        emit statusMessage(QString("Creating read-only snap in monthly (%1)").arg(midTermSnap));

        btrfs.makeSnapshot(QDir(mPaths->dailyBasePath()).absoluteFilePath(dailySnap),
                           QDir(mPaths->midTermBasePath()).absoluteFilePath(midTermSnap),
                           true /* read only */ );
    }

    // Check if snap shall be made in yearly

    // Check if snap shall be made in midterm

    QString yearlySnap = QDate::currentDate().toString("yyyy");

    if (!btrfs.isPathASubVolume(QDir(mPaths->yearlyBasePath()).absoluteFilePath(yearlySnap)))
    {
        emit statusMessage(QString("Creating read-only snap in yearly (%1)").arg(yearlySnap));
        btrfs.makeSnapshot(QDir(mPaths->midTermBasePath()).absoluteFilePath(midTermSnap),
                           QDir(mPaths->yearlyBasePath()).absoluteFilePath(yearlySnap),
                           true /* read only */ );
    }

}

void RunBackupWidget::cleanSnapshots()
{
    const int dailyRetention = 15;
    const int midTermRetention = 12;

    Btrfs btrfs;
    QStringList snaps;
    QStringList removes;

    // Clean out dailys

    emit statusMessage(QString("Clean out older daily snapshots (only %1 is preserved)").arg(dailyRetention));
    // List snaps in Daily
    snaps = btrfs.listSubvolumesIn(mPaths->dailyBasePath());
    snaps.sort();

    emit logMessage("List of sorted daily snapshots");
    foreach(QString daily, snaps)
        emit logMessage(daily);


    while(snaps.size() > dailyRetention)
        removes.append(snaps.takeFirst());

    emit logMessage(QString("Removing daily snapshots (only saves %1)").arg(dailyRetention));
    foreach(QString snap, removes)
    {
        QString status;
        QString fullpath = QDir(mPaths->dailyBasePath()).absoluteFilePath(snap);
        if (btrfs.sub_remove(fullpath))
            status = QString("Removed %1 successfully").arg(snap);
        else
            status = QString("Removed %1 with error.").arg(snap);

        emit logMessage(status);
    }


    // Clean out midterm
    emit statusMessage(QString("Clean out older monthly snapshots (only %1 is preserved)").arg(midTermRetention));

    snaps.clear();
    removes.clear();

    // List snaps in Daily
    snaps = btrfs.listSubvolumesIn(mPaths->midTermBasePath());
    snaps.sort();

    emit logMessage("List of sorted mid term snapshots");
    foreach(QString snap, snaps)
        emit logMessage(snap);


    while(snaps.size() > midTermRetention)
        removes.append(snaps.takeFirst());

    emit logMessage(QString("Removing mid term snapshots (only saves %1)").arg(midTermRetention));
    foreach(QString snap, removes)
    {
        QString status;
        QString fullpath = QDir(mPaths->midTermBasePath()).absoluteFilePath(snap);
        if (btrfs.sub_remove(fullpath))
            status = QString("Removed %1 successfully").arg(snap);
        else
            status = QString("Removed %1 with error.").arg(snap);

        emit logMessage(status);
    }

    emit statusMessage(QString("Cleaning done."));

}

void RunBackupWidget::workDone()
{

    if (mCurrentBackupJob)
        disconnect(mCurrentBackupJob);

    ui->pDoneLabel->setHidden(false);
    emit backupDone();
    emit setNextButtonEnabled(true);

}
