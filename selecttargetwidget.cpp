#include "lableddiskpartition.h"
#include "selecttargetwidget.h"
#include "ui_selecttargetwidget.h"

#include <QProcess>


#include <QAbstractButton>
#include <QUuid>
#include <QVariant>
#include <QtDebug>

SelectTargetWidget::SelectTargetWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::SelectTargetWidget)
{
    ui->setupUi(this);
}

SelectTargetWidget::~SelectTargetWidget()
{
    delete ui;
}

bool SelectTargetWidget::isTargetSelected() const
{
    return !mSelectedUuid.isEmpty();

}

QString SelectTargetWidget::targetUuid() const
{
    return mSelectedUuid;
}

void SelectTargetWidget::btnNextPressed()
{
    mSelectedUuid = ui->pPartList->currentData().toString();
    if (QUuid(mSelectedUuid).isNull())
    {
        mSelectedUuid.clear();
        emit nextState(State_Error);
    }
    else
        emit nextState(State_Prepare);


}


bool SelectTargetWidget::populateMounts()
{

    QProcess lsblk;

	lsblk.setProgram("lsblk");
    QStringList args;

    args << "-r" << "-o" << "name,label,size,uuid";
    lsblk.setArguments(args);


    lsblk.start();

    // Wait for it to start
    if(!lsblk.waitForStarted())
        return false;

    bool retval = false;
    QByteArray buffer;
    // To be fair: you only need to wait here for a bit with shutdown,
    // but I will still leave the rest here for a generic solution
    while ((retval = lsblk.waitForFinished()))
        buffer.append(lsblk.readAll());

    if (!retval) {
        qDebug() << "lsblk error:" << lsblk.errorString();
    }

    QString sBuffer = QString::fromUtf8(buffer);
    qDebug() << sBuffer;


    QStringList disks = sBuffer.split("\n").filter("TR\\x20Backup"); // .replaceInStrings("\\x20", " ");

    mPartitions.clear();
    foreach (QString oneDisk, disks)
    {
        LabledDiskPartition part(oneDisk);
        mPartitions.append(part);
    }


    qDebug() << "\n\nTR Backup Disks:";


    ui->pPartList->clear();

    foreach(LabledDiskPartition part, mPartitions)
    {
        qDebug() << part.device << ", " << part.lable << ", " << part.uuid;
        ui->pPartList->addItem(part.viewLable, QVariant(part.uuid));

    }

    if (!mPartitions.isEmpty())
        emit setNextButtonEnabled(true);
    else
        emit setNextButtonEnabled(false);

    return true;


}
