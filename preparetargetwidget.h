#ifndef PREPARETARGETWIDGET_H
#define PREPARETARGETWIDGET_H

#include "mainwindowstates.h"
#include "targetpaths.h"
#include <QWidget>

namespace Ui {
class PrepareTargetWidget;
}

class PrepareTargetWidget : public QWidget
{
        Q_OBJECT


    public:
        enum  MIDTERM { Month = 0, Quartet, Tertial} ;

    public:
        explicit PrepareTargetWidget(QWidget *parent = nullptr);
        ~PrepareTargetWidget();

        void checkAndVerify(const QString& targetDevice,
                            TargetPaths& paths);

        bool isStatusVerifiedOk();

        bool umount(TargetPaths* paths);

    public slots:
        void btnNextPressed();


    protected slots:

        bool mountAndVerify(const QString &targetDevice, TargetPaths& paths);
        bool checkCreateBaseDir(TargetPaths& paths);
        bool checkCreateTargetDirs(TargetPaths& paths);


    signals:
        void nextState(MainWindoState nextState);
        void setNextButtonEnabled(bool enable);
        void checkedAndVerifiedOk();


    protected:
        bool checkCreatePath(const QString& path);

    private:
        Ui::PrepareTargetWidget *ui;
        bool                bStatusVerifiedOK;

};

#endif // PREPARETARGETWIDGET_H
