#include "datamodel.h"

#include <QFile>
#include <QIODevice>
#include <QJsonArray>
#include <QJsonDocument>

#include <QtDebug>

DataModel::DataModel(QObject *parent)
    : QObject{parent}
{

}

bool DataModel::load(const QString& path)
{

    QFile    dataFile(path);
    dataFile.open(QIODevice::ReadOnly);

    QByteArray dataFileContent = dataFile.readAll();

    QJsonParseError     parseError;
    QJsonDocument       dataSource = QJsonDocument::fromJson(dataFileContent, &parseError);

    if (QJsonParseError::NoError == parseError.error)
    {
        QJsonObject dataRoot = dataSource.object();
        QJsonArray dataSetList =dataRoot.value("datasets").toArray();

        for(QJsonArray::const_iterator iter = dataSetList.constBegin(); iter != dataSetList.constEnd(); iter++)
        {
            QJsonObject obj = (*iter).toObject();
            mDataSets.append(new DataSet(obj));
        }

    }
    else
    {
        qDebug() << parseError.errorString();
    }

    return QJsonParseError::NoError == parseError.error;

}

const QList<DataSet*>& DataModel::dataSets() const
{
    return mDataSets;
}

DataModel::const_iterator DataModel::constBegin() const
{
    return mDataSets.constBegin();
}

DataModel::const_iterator DataModel::constEnd() const
{
    return mDataSets.constEnd();
}
