#ifndef DATASET_H
#define DATASET_H

#include <QObject>
#include <QJsonObject>

class DataSet : public QObject
{
        Q_OBJECT
    public:
        explicit DataSet(QObject *parent = nullptr);
        explicit DataSet(QJsonObject& sourceObject);

        const QString& name() const;

        const QString& path() const;

        const QStringList& excludeList() const;

        const QString& rSyncFlags() const;

    signals:

    private:
        QString     mName;
        QString     mPath;
        QString     mRSyncFlags;
        QStringList mExcludeList;

};

#endif // DATASET_H
