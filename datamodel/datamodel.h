#ifndef DATAMODEL_H
#define DATAMODEL_H

#include <QObject>

#include "dataset.h"

class DataModel : public QObject
{
        Q_OBJECT

    public:

        typedef QList<DataSet*>::const_iterator const_iterator;

    public:
        explicit DataModel(QObject *parent = nullptr);


        bool load(const QString& path);


        const QList<DataSet*>& dataSets() const;

        const_iterator constBegin() const;
        const_iterator constEnd() const;


    signals:

    private:
        QList<DataSet*>     mDataSets;

};

#endif // DATAMODEL_H
