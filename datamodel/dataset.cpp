#include "dataset.h"

#include <QJsonObject>

#include <QJsonArray>
#include <QtDebug>

DataSet::DataSet(QObject *parent)
    : QObject{parent}
{

}

DataSet::DataSet(QJsonObject& sourceObject)
{
    qDebug() << "Parsing an object from " << sourceObject;

    mName = sourceObject.value("name").toString();
    mPath= sourceObject.value("path").toString();
    mRSyncFlags = sourceObject.value("rsyncflags").toString();

    QJsonArray excludes = sourceObject.value("exclude").toArray();

    if (!excludes.isEmpty())
    {
        for(QJsonArray::const_iterator iter = excludes.constBegin(); iter != excludes.constEnd(); iter++)
        {
            mExcludeList.append((*iter).toString());
        }
    }

    qDebug() << "Parsed a Data Set:";
    qDebug() << "  mName        " << mName;
    qDebug() << "  mPath        " << mPath;
    qDebug() << "  mRSyncFlags  " << mRSyncFlags;
    qDebug() << "  mExcludeList " << mExcludeList ;

}

const QString& DataSet::name() const
{
    return mName;
}

const QString& DataSet::path() const
{
    return mPath;
}

const QStringList& DataSet::excludeList() const
{
    return mExcludeList;
}

const QString& DataSet::rSyncFlags() const
{
    return mRSyncFlags;
}
