#include "errorwidget.h"
#include "ui_errorwidget.h"

ErrorWidget::ErrorWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::ErrorWidget)
{
    ui->setupUi(this);
}

ErrorWidget::~ErrorWidget()
{
    delete ui;
}

void ErrorWidget::btnNextPressed()
{

    emit nextState(State_Quit);

}
